
UK Local Authority Coronavirus Rates Checker
============================================

This is a small script designed to act as a custom Waybar module which
scrapes the UK Government's coronavirus data API to find the infection
rates in your local area.

![Screenshot showing usage](./screenshot.png)

The above screenshot shows the data the script pulls in for the given
region: The name of the area, as defined on the Coronavirus API, the
current rolling number of cases per 100k of the population, and then the
total rolling number of cases with the change from last week as both an
absolute and a percentage.

This is the same information that is presented on the [Official
Interactive Map][2].

## Usage

### Calling from the command line

```
covid-checker [Area Type] [Area Code]
```

The Area Type most be one of:
* utla (the highest level possible - eg Devon's)
* ltla (the medium size of region - eg North Devon)
* msoa (the smallest local area - eg Barnstaple South)

The Area Code should be the unique identifying code for the Local
Authority. Some examples:
* Devon: E10000008
* North Devon: E07000043
* Barnstaple South: E02004184
* Bristol: E06000023

Codes always start with a "E" for areas of England, "W" for areas within
Wales, "S" for Scotland, and "N" for Northern Ireland.

You can use [FindThatPostCode][1] to help locate the correct code.

### Within Your WayBar config

```
{
    "modules-left": ["custom/covid"],

    "custom/covid": {
        # City of Bristol - see above for details of this line
        "exec": "covid-checker ltla E06000023",

        # Important as covid-checker always returns a Waybar's JSON format
        "return-type": "json",

        # {} will work on its own, but you may also want to add an icon or label
        "format": "{icon} {}",

        # If you want to show icons for up and down
        "format-icons": ["v", "^"]
    }
}
```

### Styling

The script returns unformatted text, so there's not a huge amount of
styling options, but if you named it `custom/covid` as shown above, you
can of course style the box with `#custom-covid`. If the rate is going
down, the class name for it will be set to `DOWN`, and it will be set to
`UP` if the rates are rising. A basic example of styling this:

```
#custom-covid.DOWN {
    color: green;
}

#custom-covid.UP {
    color: red;
}
```

## Installation

This is a single file script, so you can just download it and run it:

```
> git clone https://gitlab.com/EmilyShepherd/covid-checker.git
> sudo cp covid-checker/covid-checker /usr/bin/
```

### Dependencies

This requires the following packages:

* `dateutils`
* `jq`
* `curl`
* `bash`

#### Arch Linux

`bash` is installed by default, `curl` is in core. The final two are in
the community repo.

```
sudo pacman -S dateutils jq curl
```

#### Void Linux

`bash` is installed by default. The others are all available in the main
package repository.


```
sudo xbps-install dateutils jq curl
```




[1]: https://findthatpostcode.uk/
[2]: https://coronavirus.data.gov.uk/details/interactive-map
